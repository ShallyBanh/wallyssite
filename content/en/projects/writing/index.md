---
title: "Low Toughness of Carbon Steel Pressure Components"
weight: 3
resources:
    - src: LowToughnessofCarbonSteelPressureComponents.jpg
      params:
          weight: -100
---

A company in the oil and gas industry has reported several cases of brittle fracture in carbon steel pipe components, declaring the root cause to be low toughness in carbon steel materials: SA-105 N, SA-106 B, and SA-234 WPB. For the purpose of confidentiality, the name of the company will not be disclosed and will be labelled as company A. In response, the Alberta Boilers Safety Association (ABSA) has issued ABSA Information Bulletin No. IB16-018, restating the claims made by company A and recommending fabricators to review the carbon steel toughness requirements with designers and owners before exempting them from impact testing. Due to being a fabricator, our client is affected by the claims made by company A. In response, our client has asked us to investigate this matter, and if a problem exist, determine a solution.
