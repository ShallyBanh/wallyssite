---
title: "Failure Analysis"
weight: 1
resources:
    - src: FailureAnalysis.jpg
      params:
          weight: -100
---

A woodruff key used to hold a gear in place on the shaft of a torque converter pump has failed. This torque converter pump was in a 797 Caterpillar truck that was operating at one of the oil sands sites near Fort McMurray, and failure of the key caused shutdown of the truck for repairs. Although the woodruff key is such a small piece, its role in keeping the gear of the torque converter pump stationary on the shaft cannot be understated. It is believed that a torque converter pump has a lifetime of around 30000 hours on average. However, at around 15000 hours, these pumps are usually torn down and rebuilt to ensure that all the parts in the pump are still in good working condition and if anything needs to be replaced, it can be. When the pump with this woodruff key was rebuilt, it is believed the key wasn’t replaced and left in the pump to carry on for the remaining 15000 hours of the pump’s lifetime. When the rebuild was done, the key could have possibly been damaged in some way that wasn’t detected when put back in, or the key may possibly have not been inspected at all. Whatever the case, the key wasn’t able to hold up during the rest of the pumps expected lifetime, failing because of something that either happened during operation or during the maintenance of the pump.
Analysis through Vickers hardness testing, scanning electron microscopy (SEM), and energy dispersive x-ray spectroscopy were conducted on the fractured woodruff key of a torque converter pump to determine the cause of failure and how to mitigate this failure in the future.
