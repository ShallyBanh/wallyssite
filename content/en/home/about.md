---
title: "About"
weight: 0
---

### Facts About Me

*	Bachelor of Science in Materials Engineering from the University of Alberta
*	Registered with APEGA as an Engineer-in-Training (E.I.T.)
*	Knowledge of engineering principles and techniques including physical, chemical and extractive metallurgy, mineral processing, welding, corrosion, wear and protection, fracture mechanics, failure analysis, polymers, ceramics, thermodynamics, fluid mechanics, heat transfer, computational methods, statistics, and engineering safety and risk management
*	Experience and knowledge of welding processes, sample polishing, MTRs, destructive testing, non-destructive testing (NDT), and non-destructive evaluation (NDE)
*	Experience with the quality control (QC), quality assurance (QA) and knowledge of standards and codes from standards org. such as: ASTM, ASME, CSA, AWS, ANSI, API, and NACE
*	Completed technical design projects, developing skills in drafting, process flow design, cost estimation, project planning and scheduling, risk assessment, and understanding of plant safety
*	Excellent verbal and written communication skills with the ability to work under pressure, prioritize task, and meet deadlines
*	Experience with sales consisting of RFQ, sourcing products, and expediting orders
*	Excellent computer skills and knowledge of MS Office (Word, Excel, PowerPoint, & Outlook)
*	Experience using AutoCAD for drafting, Visio for process flow design, and ERP software


## CORE STRENGTH/TECHNICAL SKILLS
*   Material Science: Metals, Ceramics, & Polymers
*   Physical, Chemical, & Extractive Metallurgy
*   Fracture Mechanics, Wear, & Corrosion
*   Welding Metallurgy & Heat Transfer
*   AutoCAD, SolidWorks, MATLAB, & C++
*   Microsoft Office: Word, Excel, Outlook, Visio, PowerPoint, etc
*   Standards Orgs: ASME, ASTM, CSA, API, & NACE
*   Failure Analysis & Material Testing
*   Destructive & Non-Destructive Testing (NDT)
*   Process Flow Design & Analysis
*   QA/QC Documentations & Lab Reports
*   Design of Experiments (DOE)
*   Safety & Risk Management
*   Analytical & Creative Process Thinking
*   Team Leadership & Project Management
*   Project Planning & Scheduling, Cost Estimation, Technical Report Writing, & Technical Presentations
*   Experience with RFQs, procurement, and MTRs for process piping material